import React, { useState,createContext } from 'react';
import Todolist from './TodoList'

export const RootContext = createContext()

const Context = () => {

    const [todo, addTodo] = useState([])
    const [value, setValue] = useState('')

    onChangeInput = (value) => {
        setValue(value)
    }

    deleteItem = (key) => {
        let data = todo.filter((_, index) => index !== key)
        addTodo(data)
    }

    addItems = () => {
        addTodo(todo => [...todo,{value: value, date: getDate()}])
        setValue('')
    }

    return (
        <RootContext.Provider value={{
            value,
            todo,
            deleteItem,
            addItems,
            onChangeInput
        }}>
            <Todolist/>
        </RootContext.Provider>
    )

}

function getDate(){

    let Tanggal = new Date()
    let date = Tanggal.getDate()
    let month = Tanggal.getMonth()+1
    let year = Tanggal.getFullYear()
    
    return fulldate = `${date}/${month}/${year}`
}

export default Context