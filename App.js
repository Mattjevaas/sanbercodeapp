/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Intro from './src/screens/Tugas1/intro'
import Biodata from './src/screens/Tugas2/Biodata'
import Todolist from './src/screens/Tugas3/TodoList'
import Todolists from './src/screens/Tugas4/index'
import Navigation from './src/screens/Tugas5/navigation'

class App extends React.Component {
  render(){
    return(
      // <Intro/>
      // <Biodata/>
      <SafeAreaView style={styles.container}>
        {/* <Todolists/> */}
        <Navigation/>
      </SafeAreaView>
    )
  } 
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});

export default App;
