import React, { useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Dimensions,
    Image
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import api from '../../api'

const DEVICE = Dimensions.get('window')

function Biodata({navigation}){


    const onClickLogout = async () => {
        try{
            await AsyncStorage.removeItem("token")
            navigation.navigate('Login')
        }catch(err){
            console.log(err)
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.uppderHeader}>
                <Image style={styles.avatarImage} source={require('../../assets/images/avatar.png')}/>
                <Text style={styles.personName}>Johanes Wiku Sakti</Text>
            </View>
            <View style={styles.boxContainer}>
                <View style={styles.box}>
                    <View style={styles.itemContainer}>
                        <Text>Tanggal lahir</Text>
                        <Text>20 Juli 1999</Text>
                    </View>
                    <View style={styles.itemContainer}>
                        <Text>Jenis Kelamin</Text>
                        <Text>Laki - laki</Text>
                    </View>
                    <View style={styles.itemContainer}>
                        <Text>Hobi</Text>
                        <Text>Main Game</Text>
                    </View>
                    <View style={styles.itemContainer}>
                        <Text>No. Telp</Text>
                        <Text>082280131707</Text>
                    </View>
                    <View style={styles.itemContainer}>
                        <Text>Email</Text>
                        <Text>johaneswiku@protonmail.ch</Text>
                    </View>
                    <TouchableOpacity style={styles.button} onPress={()=>onClickLogout()}>
                        <Text style={{color: 'white'}}>LOGOUT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
    },
    uppderHeader: {
        backgroundColor: '#52c8ff',
        height: DEVICE.height * 0.4 ,
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatarImage: {
        height: 160,
        width: 160,
        borderRadius: 100,
        marginTop: 20
    },
    personName: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        marginTop: 20
    },
    box:{
        backgroundColor: 'white',
        width: DEVICE.width * 0.85,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        padding: 20,
    },
    boxContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: DEVICE.height * -0.04
    },
    itemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10
    },
    button: {
        borderRadius: 2,
        marginVertical: 10,
        backgroundColor: '#3EC6FF',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default Biodata