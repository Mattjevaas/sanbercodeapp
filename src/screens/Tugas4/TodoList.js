import React, { useContext } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TextInput,
    TouchableOpacity,
    FlatList
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RootContext} from '../Tugas4'

const DEVICE = Dimensions.get('window')

function Todolist(){

    const state = useContext(RootContext)

    const Item = ({item,index}) => (
        <View key={index} style={styles.contentContainer}>
            <View style={styles.insideContainer}>
                <Text>{item.date}</Text>
                <Text>{item.value}</Text>
            </View>

            <TouchableOpacity onPress={()=>state.deleteItem(index)}>
                <Icon name="trash-can-outline" size={20}/>
            </TouchableOpacity>
        </View>
    )

    return (
        <View style={styles.container}>
            <Text>Masukan Todolist</Text>
            <View style={styles.fieldContainer}>
                <TextInput
                    style={styles.textInput}
                    placeholder="Input Here"
                    value={state.value}
                    onChangeText={text => state.onChangeInput(text)}
                    
                />
                <TouchableOpacity style={styles.buttonPlus} onPress={()=>state.addItems()}>
                    <Text style={{fontSize: 20}}>+</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                data={state.todo}
                renderItem={Item}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        padding: 10
    },
    textInput: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        width: DEVICE.width * 0.85
    },
    fieldContainer: {
        marginVertical: 10,
        flexDirection: 'row',
    },
    buttonPlus: {
        backgroundColor: '#52c8ff',
        justifyContent: 'center',
        alignItems: 'center',
        width: 40,
        marginHorizontal: 5
    },
    contentContainer:{
        borderRadius: 5,
        borderWidth: 3,
        borderColor: '#bababa',
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 5
    },
})

Icon.loadFont();
export default Todolist