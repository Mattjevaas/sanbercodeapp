import React, { useState } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import api from '../../api'

function Login({ navigation }) {

    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const [failed, setFailed] = useState(false)

    const saveToken = async (token) => {
        try{
            await AsyncStorage.setItem("token",token)
        }catch(err){
            console.log(err)
        }
    }

    const Toast = () => {
        if(failed){
            return <View style={styles.toastBox}><Text style={{color: 'white'}}>GAGAL LOGIN !</Text></View>
        }else{
            return <View></View>
        }
    }

    const onClickLogin = ()=>{
        let data = {
            email: email,
            password: password
        }

        Axios.post(`${api}/login`,data, {
            timeout: 20000
        }).then((res)=>{
            saveToken(res.data.token)
            navigation.navigate('Profile')
        }).catch((err)=>{
            setFailed(true)
            setTimeout(()=>{
                setFailed(false)
            },5000)
            console.log(err)
        })
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <Image source={require('../../assets/images/logo.jpg')} />
            <View style={styles.fieldContainer}>
                <View style={styles.fieldGroup}>
                    <Text style={styles.titleField} >Username</Text>
                    <TextInput 
                        style={styles.textInput} 
                        placeholder="Username or Email"
                        onChangeText={(text)=>setEmail(text)}
                    />
                </View>
                <View style={styles.fieldGroup}>
                    <Text style={styles.titleField} >Password</Text>
                    <TextInput 
                        style={styles.textInput} 
                        placeholder="Password"
                        secureTextEntry={true}
                        onChangeText={(text)=>setPassword(text)}
                    />
                </View>
                <View>
                    <TouchableOpacity style={styles.buttonOne} onPress={()=>onClickLogin()}>
                        <Text style={{color: 'white'}}>LOGIN</Text>
                    </TouchableOpacity>
                    {/* <View style={styles.separatorContainer}>
                        <View style={styles.separator}/>
                        <Text>OR</Text>
                        <View style={styles.separator}/>
                    </View>
                    <TouchableOpacity style={styles.buttonTwo}>
                        <Text style={{color: 'white'}}>LOGIN WITH GOOGLE</Text>
                    </TouchableOpacity> */}
                </View>
            </View>
            <View>
                <Toast/>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <Text>Belum mempunyai akun ? </Text>
                    <TouchableOpacity>
                        <Text style={{color: 'darkblue'}}>Buat Akun</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({

    container:{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'space-between'
    },
    fieldContainer: {
        marginHorizontal: 20,
        marginTop: -100
    },
    textInput: {
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        padding: 5,
        marginVertical: 10
    },
    titleField :{
        fontWeight: 'bold'
    },
    fieldGroup: {
        marginVertical: 10,
    },
    buttonOne : {
        backgroundColor: '#00b7db',
        justifyContent: 'center',
        alignItems: 'center',
        height: 38,
        borderRadius: 3,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 0.5

    },
    buttonTwo: {
        backgroundColor: '#d93a00',
        justifyContent: 'center',
        alignItems: 'center',
        height: 38,
        borderRadius: 3,
        shadowOffset: {width: 1, height: 1},
        shadowOpacity: 0.5
    },
    separator: {
        height: 2,
        width: 170,
        backgroundColor: 'grey',
        marginVertical: 40,
        marginHorizontal: 10,
    },
    separatorContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    toastBox: {
        backgroundColor: '#f7bd0f',
        height: 40,
        width: 150,
        borderRadius: 2,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    }


})

export default Login;