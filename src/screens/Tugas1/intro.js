import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';

export default class Intro extends React.Component {

    render(){
        return (
            <View style={styles.container}>
                <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})