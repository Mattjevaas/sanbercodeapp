import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Dimensions,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const DEVICE = Dimensions.get('window')

function Todolist(){

    const [todo, addTodo] = useState([])
    const [value, setValue] = useState('')

    const deleteItem = key => () => {
        let data = todo.filter((_, index) => index !== key)
        addTodo(data)
    }

    const addItems = () => {
        addTodo(todo => [...todo,[value,getDate()]])
    }

    const Item = todo.map((val,key) => (
            <View key={key} style={styles.contentContainer}>
                <View style={styles.insideContainer}>
                    <Text>{val[1]}</Text>
                    <Text>{val[0]}</Text>
                </View>

                <TouchableOpacity onPress={deleteItem(key)}>
                    <Icon name="trash-can-outline" size={20}/>
                </TouchableOpacity>
            </View>
        )
    )

    return (
        <View style={styles.container}>
            <Text>Masukan Todolist</Text>
            <View style={styles.fieldContainer}>
                <TextInput
                    style={styles.textInput}
                    placeholder="Input Here"
                    value={value}
                    onChangeText={(text)=>setValue(text)}
                />
                <TouchableOpacity style={styles.buttonPlus} onPress={addItems}>
                    <Text style={{fontSize: 20}}>+</Text>
                </TouchableOpacity>
            </View>
            {Item}
        </View>
    )
}

function getDate(){

    let Tanggal = new Date()
    let date = Tanggal.getDate()
    let month = Tanggal.getMonth()+1
    let year = Tanggal.getFullYear()
    
    return fulldate = `${date}/${month}/${year}`
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        padding: 10
    },
    textInput: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        width: DEVICE.width * 0.85
    },
    fieldContainer: {
        marginVertical: 10,
        flexDirection: 'row',
    },
    buttonPlus: {
        backgroundColor: '#52c8ff',
        justifyContent: 'center',
        alignItems: 'center',
        width: 40,
        marginHorizontal: 5
    },
    contentContainer:{
        borderRadius: 5,
        borderWidth: 3,
        borderColor: '#bababa',
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 5
    },
})

Icon.loadFont();
export default Todolist